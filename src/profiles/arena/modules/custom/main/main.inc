<?php

function _main_language_provider_callback($languages) {
    if (!drupal_is_cli()) {
        require_once $_SERVER['DOCUMENT_ROOT'] . '/includes/path.inc';

        // Get current path without language prefix
        list($language, $path) = language_url_split_prefix(isset($_GET['q']) ? $_GET['q'] : NULL, $languages);

        // Return en-en for admin and account access, see main_init()
        if (MainPrivateController::isMySpace() || strpos($path, 'admin') === 0 || strpos($path, 'manage') === 0
          || strpos($path, 'supplier') === 0 || strpos($path, 'user') === 0) {
            return 'en-en';
        }
    }
}
