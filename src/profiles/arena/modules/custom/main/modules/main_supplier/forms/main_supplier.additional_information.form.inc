<?php

function _main_supplier_additional_information_form($form, &$form_state)
{
    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);
    if (isset($profile->field_additional_information[LANGUAGE_NONE][0]['value'])) {
        $values = json_decode($profile->field_additional_information[LANGUAGE_NONE][0]['value'], TRUE);

    }

    $types = array('shareholder', 'brand', 'license', 'affiliate');
    foreach ($types as $type) {
        if (isset($form_state['storage'][$type . '_count'])) {
            if (isset($form_state['triggering_element']['#parents'][0])) {
                if ($form_state['triggering_element']['#parents'][0] == 'add_' . $type) {
                    $form_state['storage'][$type . '_count']++;
                } elseif ($form_state['triggering_element']['#parents'][0] == 'remove_' . $type) {
                    $form_state['storage'][$type . '_count']--;
                }
            }
        } else {
            switch ($type) {
                case 'shareholder':
                    if (isset($values['shareholder'])) {
                        $form_state['storage'][$type . '_count'] = count($values['shareholder']);
                    }
                    break;
                case 'brand':
                    if (isset($values['brand'])) {
                        $form_state['storage'][$type . '_count'] = count($values['brand']);
                    }
                    break;
                case 'license':
                    if (isset($values['license'])) {
                        $form_state['storage'][$type . '_count'] = count($values['license']);
                    }
                    break;
                case 'affiliate':
                    if (isset($values['affiliate'])) {
                        $form_state['storage'][$type . '_count'] = count($values['affiliate']);
                    }
                    break;
            }

            if (!isset($form_state['storage'][$type . '_count'])) {
                $form_state['storage'][$type . '_count'] = 1;
            }
        }
    }

    $form['fieldset_1'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        '#title' => 'Currency',
        'currency' => array(
            '#type' => 'select',
            '#title' => 'Currency',
            '#description' => 'This currency will be the one automatically used throughout the form.',
            '#empty_value' => '',
            '#options' => array(
                'USD' => 'Dollar',
                'EUR' => 'Euro',
            ),
            '#default_value' => isset($values['currency']) ? $values['currency'] : '',
            '#required' => TRUE,
        ),
    );

    $form['fieldset_2'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        '#title' => 'Capital',
    );
    $form['fieldset_2']['capital'] = array(
        '#type' => 'textfield',
        '#title' => 'Capital',
        '#description' => 'Capital in local currency',
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
        '#default_value' => isset($values['capital']) ? $values['capital'] : '',
    );
    $form['fieldset_2']['capital_currency'] = array(
        '#type' => 'textfield',
        '#title' => 'Capital currency',
        '#size' => 60,
        '#maxlength' => 128,
        '#required' => TRUE,
        '#default_value' => isset($values['capital_currency']) ? $values['capital_currency'] : '',
    );

    $form['fieldset_2']['shareholder'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="shareholder"><span class="panel-subtitle">Shareholdering structure</span>',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_2']['add_shareholder'] = array(
        '#type' => 'button',
        '#value' => 'Add shareholder',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_add_shareholder',
            'wrapper' => 'shareholder',
        ),
    );
    $form['fieldset_2']['remove_shareholder'] = array(
        '#type' => 'button',
        '#value' => 'Remove shareholder',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_remove_shareholder',
            'wrapper' => 'shareholder',
        ),
    );
    $shareholder_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'shareholder_name' => array(
            '#type' => 'textfield',
            '#title' => 'Shareholder name',
        ),
        'shareholder_share' => array(
            '#type' => 'textfield',
            '#title' => 'Share owned',
            '#description' => 'In percentage'
        ),
    );
    for ($i = 1; $i <= $form_state['storage']['shareholder_count']; $i++) {
        $shareholder_form['shareholder_name']['#default_value'] = isset($values['shareholder'][$i - 1]['shareholder_name']) ? $values['shareholder'][$i - 1]['shareholder_name'] : '';
        $shareholder_form['shareholder_share']['#default_value'] = isset($values['shareholder'][$i - 1]['shareholder_share']) ? $values['shareholder'][$i - 1]['shareholder_share'] : '';
        $form['fieldset_2']['shareholder'][] = $shareholder_form;
    }

    $form['fieldset_3'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        '#title' => 'Brands & Licenses',
    );

    $brand_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'brand_name' => array(
            '#type' => 'textfield',
            '#title' => 'Brand name',
        ),
    );
    $form['fieldset_3']['brand'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="brand"><span class="panel-subtitle">Brands owned</span>',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_3']['add_brand'] = array(
        '#type' => 'button',
        '#value' => 'Add brand',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_add_brand',
            'wrapper' => 'brand',
        ),
    );
    $form['fieldset_3']['remove_brand'] = array(
        '#type' => 'button',
        '#value' => 'Remove brand',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_remove_brand',
            'wrapper' => 'brand',
        ),
    );
    for ($i = 1; $i <= $form_state['storage']['brand_count']; $i++) {
        $brand_form['brand_name']['#default_value'] = isset($values['brand'][$i - 1]['brand_name']) ? $values['brand'][$i - 1]['brand_name'] : '';
        $form['fieldset_3']['brand'][] = $brand_form;
    }

    $license_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'license_name' => array(
            '#type' => 'textfield',
            '#title' => 'License name',
        ),
    );
    $form['fieldset_3']['license'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="license"><span class="panel-subtitle">Licenses owned</span>',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_3']['add_license'] = array(
        '#type' => 'button',
        '#value' => 'Add license',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_add_license',
            'wrapper' => 'license',
        ),
    );
    $form['fieldset_3']['remove_license'] = array(
        '#type' => 'button',
        '#value' => 'Remove license',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_remove_license',
            'wrapper' => 'license',
        ),
    );
    for ($i = 1; $i <= $form_state['storage']['license_count']; $i++) {
        $license_form['license_name']['#default_value'] = isset($values['license'][$i - 1]['license_name']) ? $values['license'][$i - 1]['license_name'] : '';
        $form['fieldset_3']['license'][] = $license_form;
    }

    $form['fieldset_4'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        '#title' => 'List of subsidiaries',
    );

    $affiliate_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'company_name' => array(
            '#type' => 'textfield',
            '#title' => 'Subsidiary company name',
        ),
        'country' => array(
            '#type' => 'select',
            '#title' => 'Country',
            '#description' => 'The countries on the top of the list correspond to our allies\' markets',
            '#empty_value' => '',
            '#options' => country_get_list(),
        ),
    );

    $product_nomenclature = taxonomy_vocabulary_machine_name_load('product_nomenclature');
    $products_categories = taxonomy_get_tree($product_nomenclature->vid, 0, 1);

    foreach ($products_categories as $products_category) {
        $tree = taxonomy_get_tree($product_nomenclature->vid, $products_category->tid, 1);
        $options = array();
        foreach ($tree as $branch) {
            $options[$branch->tid] = $branch->name;
        }
        $affiliate_form['product_families'][] = array(
            '#type' => 'checkboxes',
            '#title' => $products_category->name,
            '#options' => $options,
        );
    }

    $form['fieldset_4']['affiliate'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="affiliate"><span class="panel-subtitle">Subsidiaries</span>',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_4']['add_affiliate'] = array(
        '#type' => 'button',
        '#value' => 'Add affiliate',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_add_affiliate',
            'wrapper' => 'affiliate',
        ),
    );
    $form['fieldset_4']['remove_affiliate'] = array(
        '#type' => 'button',
        '#value' => 'Remove affiliate',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_additional_information_form_ajax_remove_affiliate',
            'wrapper' => 'affiliate',
        ),
    );
    for ($i = 1; $i <= $form_state['storage']['affiliate_count']; $i++) {
        $affiliate_form['company_name']['#default_value'] = isset($values['affiliate'][$i - 1]['company_name']) ? $values['affiliate'][$i - 1]['company_name'] : '';
        $affiliate_form['country']['#default_value'] = isset($values['affiliate'][$i - 1]['country']) ? $values['affiliate'][$i - 1]['country'] : '';
        $j = 0;
        foreach ($products_categories as $products_category) {
            $affiliate_form['product_families'][$j]['#default_value'] = isset($values['affiliate'][$i - 1]['product_families'][$j]) ? $values['affiliate'][$i - 1]['product_families'][$j] : array();
            $j++;
        }
        $form['fieldset_4']['affiliate'][] = $affiliate_form;
    }

    $form['fieldset_5'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        '#title' => 'Company turnover',
    );

    $year_form = array(
        'total_turnover' => array(
            '#type' => 'textfield',
            '#title' => 'Total turnover',
        ),
        'export_percentage' => array(
            '#type' => 'textfield',
            '#title' => 'Export turnover %',
        ),
        'investment' => array(
            '#type' => 'textfield',
            '#title' => 'Total investment in value',
        ),
        'investment_in_rd' => array(
            '#type' => 'textfield',
            '#title' => 'R&D investment',
        ),
    );

    $yearId = date('Y');
    $yearId = (int)$yearId;

    for ($i = 0; $i < 5; $i++) {
        $current_year = $yearId - $i;
        $index = 'year-' . $current_year;
        if (isset($values[$index])) {
            $year = $values[$index];
            $year_form['total_turnover']['#default_value'] = $year['year']['total_turnover'];
            $year_form['export_percentage']['#default_value'] = $year['year']['export_percentage'];
            $year_form['investment']['#default_value'] = $year['year']['investment'];
            $year_form['investment_in_rd']['#default_value'] = $year['year']['investment_in_rd'];
        }
        $form['fieldset_5'][$index] = array(
            '#type' => 'container',
            '#tree' => TRUE,
            'title' => array(
                '#markup' => ($yearId - $i) . ($i == 0 ? ' estimated' : ''),
                '#prefix' => '<span class="panel-subtitle">',
                '#suffix' => '</span>',
            ),
            'year' => $year_form
        );
    }

    return _main_supplier_formalize($form);
}


/**
 * Form validate callback.
 */
function _main_supplier_additional_information_form_validate($form, &$form_state)
{
    $values = $form_state['values'];

    //Shareholders
    if (isset($values['shareholder'])) {
        foreach ($values['shareholder'] as $id => $shareholder) {
            if (!empty($shareholder['shareholder_name']) || !empty($shareholder['shareholder_share'])) {
                if (empty($shareholder['shareholder_name'])) {
                    form_set_error('shareholder][' . $id . '][shareholder_name', t('Shareholder name field is required.'));
                }
                $shareholder_share = $shareholder['shareholder_share'];
                if (!_main_supplier_validate_numeric_positive($shareholder_share, 'number')) {
                    form_set_error('shareholder][' . $id . '][shareholder_share', t('Invalid "Share owned" value, you need to give a number.'));
                }
            }
        }
    }

    // Affiliates
    if (isset($values['affiliate'])) {
        foreach ($values['affiliate'] as $id => $affiliate) {
            // Merge nomenclature
            $affiliate_nomenclature = array_merge($affiliate['product_families'][0], $affiliate['product_families'][1], $affiliate['product_families'][2], $affiliate['product_families'][3]);
            // Remove unselect term
            $affiliate_nomenclature = array_filter($affiliate_nomenclature);
            if (!empty($affiliate_nomenclature) || !empty($affiliate['affiliate_company_name'])) {
                if (empty($affiliate['company_name'])) {
                    form_set_error('affiliate][' . $id, t('Affiliate "Company name" field is required.'));
                }
                if (empty($affiliate_nomenclature)) {
                    form_set_error('affiliate][' . $id, t('Affiliate "Product categories" field is required.'));
                }
            }
        }
    }

    // Company global turnover
    $yearId = date('Y');
    $yearId = (int)$yearId;
    $fields_infos = array(
        array('name' => 'total_turnover', 'type' => 'integer'), 
        array('name' => 'export_percentage', 'type' => 'number'), 
        array('name' => 'investment', 'type' => 'integer'),
        array('name' => 'investment_in_rd', 'type' => 'integer'),
    );
    for ($i = 0; $i < 5; $i++) {
        $currentYear = $yearId - $i;
        $index = 'year-' . $currentYear;

        foreach ($fields_infos as $field_infos) {

            if (isset($values[$index]['year'][$field_infos['name']]) && !empty($values[$index]['year'][$field_infos['name']])) {
                $value = $values[$index]['year'][$field_infos['name']];
                if (!_main_supplier_validate_numeric_positive($value, $field_infos['type'])) {
                    form_set_error($index . '][year][' . $field_infos['name'], t('Invalid "@fieldname" value for @groupname, you need to give a @type.',
                        array(
                            '@fieldname' => $form['fieldset_5'][$index]['year'][$field_infos['name']]['#title'],
                            '@groupname' => $form['fieldset_5'][$index]['title']['#markup'],
                            '@type' => $field_infos['type'],
                        )
                    ));
                }
            }
        }
    }
}

//submit
function _main_supplier_additional_information_form_submit($form, &$form_state)
{
    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);
    $w_profile = entity_metadata_wrapper('node', $profile);
    $values = $form_state['values'];

    foreach (array('submit', 'form_build_id', 'form_token', 'form_id', 'op') as $value) {
        unset($values[$value]);
    }

    $w_profile->field_additional_information = json_encode($values);
    $w_profile->save();

    drupal_set_message('Information has been saved.');

    drupal_goto('supplier/market-client-information');
}

/**********
 ** AJAX **
 **********/
function _main_supplier_additional_information_form_ajax_add_shareholder($form, &$form_state)
{
    return $form['fieldset_2']['shareholder'];
}

function _main_supplier_additional_information_form_ajax_remove_shareholder($form, &$form_state)
{
    return $form['fieldset_2']['shareholder'];
}

function _main_supplier_additional_information_form_ajax_add_brand($form, &$form_state)
{
    return $form['fieldset_3']['brand'];
}

function _main_supplier_additional_information_form_ajax_remove_brand($form, &$form_state)
{
    return $form['fieldset_3']['brand'];
}

function _main_supplier_additional_information_form_ajax_add_license($form, &$form_state)
{
    return $form['fieldset_3']['license'];
}

function _main_supplier_additional_information_form_ajax_remove_license($form, &$form_state)
{
    return $form['fieldset_3']['license'];
}

function _main_supplier_additional_information_form_ajax_add_affiliate($form, &$form_state)
{
    return $form['fieldset_4']['affiliate'];
}

function _main_supplier_additional_information_form_ajax_remove_affiliate($form, &$form_state)
{
    return $form['fieldset_4']['affiliate'];
}
