<?php

/**
 * Check if value is a integer and positive.
 */
function _main_supplier_validate_integer_positive($value)
{
    if (!is_numeric($value) || intval($value) != $value || $value < 0) {
        return FALSE;
    }
    return TRUE;
}

/**
 * Check if value is a float and positive.
 */
function _main_supplier_validate_float_positive($value)
{
    if (!is_numeric($value) || floatval($value) != $value || $value < 0) {
        return FALSE;
    }
    return TRUE;
}

/**
 * Check if value is a a numeric value having the given type and is positive.
 */
function _main_supplier_validate_numeric_positive($value, $type='integer')
{
    switch ($type){
        case 'integer':
            return _main_supplier_validate_integer_positive($value);
        case 'number':
            return _main_supplier_validate_float_positive($value);
    }
    return FALSE;
}

//util
/**
 * @param $form
 * @return mixed
 */
function _main_supplier_formalize($form)
{
    $form['formalize_fieldset_1'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        '#weight' => -1000,
    );
    $form['formalize_fieldset_1']['title'] = array(
        '#type' => 'markup',
        '#markup' => drupal_get_title(),
        '#prefix' => '<h1>',
        '#suffix' => '</h1>'
    );

    $form['formalize_fieldset_2'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        '#weight' => 1000,
    );
    $form['formalize_fieldset_2']['warning'] = array(
        '#type' => 'markup',
        '#prefix' => '<span class="disclaimer">',
        '#suffix' => '</span>',
        '#markup' => 'With this you certify to A.R.E.N.A., that the information supplied on this form is true and correct. All fields marked with an asterisk (*) are required.',
        '#weight' => 1000,
    );

    $nid = MainSupplierAPI::getCurrentNid();
    if (MainSupplierAPI::isProfileValidated($nid)) {
        $text = 'Save and next';
    } else {
        $text = 'Save';
    }
    $form['formalize_fieldset_2']['submit'] = array(
        '#type' => 'submit',
        '#value' => $text,
    );

    return $form;
}


function _main_supplier_submit_for_validation_form($form, &$form_state)
{
    $disabled = FALSE;
    $nid = MainSupplierAPI::getCurrentNid();
    if ($nid == 0) {
        $disabled = TRUE;
    } else {
        $profile = node_load($nid);
        if (!isset($profile->field_moderation_status[LANGUAGE_NONE][0]['value'])) {
            $disabled = TRUE;
        } elseif ($profile->field_moderation_status[LANGUAGE_NONE][0]['value'] == MainSupplierAPI::MODERATION_STATUS_NEW) {
            if (!MainSupplierAPI::isStepValid(1) || !MainSupplierAPI::isStepValid(2) || !MainSupplierAPI::isStepValid(3)) {
                $disabled = TRUE;
            }
        }
    }

    $form['submit'] = array(
        '#type' => 'submit',
        '#attributes' => array('class' => array($disabled ? 'disabled' : '')),
        '#value' => 'Submit for validation'
    );
    return $form;
}

function _main_supplier_submit_for_validation_form_validate($form, &$form_state)
{
    $nid = MainSupplierAPI::getCurrentNid();

    $error = FALSE;
    if ($nid == 0) {
        $error = TRUE;
    } else {
        $profile = node_load($nid);
        if (!isset($profile->field_moderation_status[LANGUAGE_NONE][0]['value'])) {
            $error = TRUE;
        } elseif ($profile->field_moderation_status[LANGUAGE_NONE][0]['value'] == MainSupplierAPI::MODERATION_STATUS_NEW) {
            if (!MainSupplierAPI::isStepValid(1) || !MainSupplierAPI::isStepValid(2) || !MainSupplierAPI::isStepValid(3)) {
                $error = TRUE;
            }
        }
    }

    if ($error) {
        form_set_error('submit', 'You cannot submit the form until all tabs are fully filled.');
    }
}

function _main_supplier_submit_for_validation_form_submit($form, &$form_state)
{
    $nid = MainSupplierAPI::getCurrentNid();

    $profile = node_load($nid);
    $profile->field_moderation_status[LANGUAGE_NONE][0]['value'] = MainSupplierAPI::MODERATION_STATUS_AWAITING_PROFILE_VALIDATION;
    node_save($profile);

    MainSupplierAPI::resetCurrentData();

    drupal_set_message('Your profile has been submitted for validation. You will be contacted by A.R.E.N.A. within one week.');
    drupal_goto('');
}
