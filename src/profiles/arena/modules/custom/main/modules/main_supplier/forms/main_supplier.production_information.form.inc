<?php

function _main_supplier_production_information_form($form, &$form_state) {

    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);

    if (isset($profile->field_production_information[LANGUAGE_NONE][0]['value'])) {
        $values = json_decode($profile->field_production_information[LANGUAGE_NONE][0]['value'], TRUE);
    }

    $types = array('human_resource', 'production_capacity', 'storage_capacity', 'factory_detail');
    foreach($types as $type) {
        if (isset($form_state['storage'][$type . '_count'])) {
            if (isset($form_state['triggering_element']['#parents'][0])) {
                if ($form_state['triggering_element']['#parents'][0] == 'add_' . $type) {
                    $form_state['storage'][$type . '_count']++;
                } elseif ($form_state['triggering_element']['#parents'][0] == 'remove_' . $type) {
                    $form_state['storage'][$type . '_count']--;
                }
            }
        } else {
            if (isset($values[$type]) && sizeof($values[$type]) > 1) {
                $form_state['storage'][$type . '_count'] = sizeof($values[$type]);
            }
            else {
                $form_state['storage'][$type . '_count'] = 1;
            }
        }
    }

    $human_resource_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'type' => array(
            '#type' => 'select',
            '#title' => 'Type',
            '#empty_value' => '',
            '#required' => TRUE,
            '#options' => array(
                'hr' => 'Human resources',
                'am' => 'Administration / Management',
                'pr' => 'Production',
                'qa' => 'Quality',
                'rd' => 'R&D'
            ),
        ),
        'employees' => array(
            '#type' => 'textfield',
            '#title' => 'Employees',
        ),
        'management' => array(
            '#type' => 'textfield',
            '#title' => 'Management',
        ),
    );
    $form['fieldset_1'] = array(
        '#type' => 'fieldset',
        '#title' => 'Human resources',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $form['fieldset_1']['human_resource'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="human-resource"><span class="panel-subtitle">Please fill in the corresponding number of employees by department</span>',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_1']['add_human_resource'] = array(
        '#type' => 'button',
        '#value' => 'Add human resource',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_production_information_form_ajax_add_human_resource',
            'wrapper' => 'human-resource',
        ),
    );
    $form['fieldset_1']['remove_human_resource'] = array(
        '#type' => 'button',
        '#value' => 'Remove human resource',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_production_information_form_ajax_remove_human_resource',
            'wrapper' => 'human-resource',
        ),
    );
    for ($i = 0; $i < $form_state['storage']['human_resource_count']; $i++) {
        $current_human_resource_form = $human_resource_form;
        $current_human_resource_form['type']['#default_value'] = isset($values['human_resource'][$i]['type']) ? $values['human_resource'][$i]['type'] : '';
        $current_human_resource_form['employees']['#default_value'] = isset($values['human_resource'][$i]['employees']) ? $values['human_resource'][$i]['employees'] : '';
        $current_human_resource_form['management']['#default_value'] = isset($values['human_resource'][$i]['management']) ? $values['human_resource'][$i]['management'] : '';
        $form['fieldset_1']['human_resource'][$i] = $current_human_resource_form;
    }

    $profile_nid = MainSupplierAPI::getCurrentNid();
    $product_subfamilies = MainSupplierAPI::getProductSubFamilies($profile_nid);
    $units_of_measure = array(
        'ton' => 'tons',
        'lfm' => 'LFM',
        'm2' => 'm2',
        'm3' => 'm3',
        'l' => 'liters',
        'count' => 'pieces'
    );
    
    $production_capacity_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'product_subfamilies' => array(
            '#type' => 'select',
            '#title' => 'Product subfamilies',
            '#empty_value' => '',
            '#required' => TRUE,
            '#options' => $product_subfamilies,
        ),
        'manufacturer_share' => array(
            '#type' => 'textfield',
            '#title' => 'Manufacturer',
            '#description' => 'in % of total activity',
        ),
        'trader_share' => array(
            '#type' => 'textfield',
            '#title' => 'Trader',
            '#description' => 'in % of total activity',
        ),
        'agent_share' => array(
            '#type' => 'textfield',
            '#title' => 'Exclusive / Non exclusive Agent',
            '#description' => 'in % of total activity',
        ),
        'owned_production_unit_annual' => array(
            '#type' => 'textfield',
            '#title' => 'Owned Production Unit',
            '#description' => 'annual production capacity',
        ),
        'owned_production_unit_annual_measure' => array(
            '#type' => 'select',
            '#title' => 'Owned Production Unit measure',
            '#empty_value' => '',
            '#options' => $units_of_measure,
        ),
        'owned_production_unit_last_year' => array(
            '#type' => 'textfield',
            '#title' => 'Owned Production Unit',
            '#description' => 'last year unit sold',
        ),
        'owned_production_unit_last_year_measure' => array(
            '#type' => 'select',
            '#title' => 'Owned Production Unit measure',
            '#empty_value' => '',
            '#options' => $units_of_measure,
        ),
        'owned_production_unit_last_year_trading_only' => array(
            '#type' => 'textfield',
            '#title' => 'Owned Production Unit',
            '#description' => 'last year trading only',
        ),
        'owned_production_unit_last_year_trading_only_measure' => array(
            '#type' => 'select',
            '#title' => 'Owned Production Unit measure',
            '#empty_value' => '',
            '#options' => $units_of_measure,
        ),
    );

    $form['fieldset_2'] = array(
        '#type' => 'fieldset',
        '#title' => 'Production capacity',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $form['fieldset_2']['production_capacity'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="production-capacity">',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_2']['add_production_capacity'] = array(
        '#type' => 'button',
        '#value' => 'Add production capacity',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_production_information_form_ajax_add_production_capacity',
            'wrapper' => 'production-capacity',
        ),
    );
    $form['fieldset_2']['remove_production_capacity'] = array(
        '#type' => 'button',
        '#value' => 'Remove production capacity',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_production_information_form_ajax_remove_production_capacity',
            'wrapper' => 'production-capacity',
        ),
    );
    for ($i = 0; $i < $form_state['storage']['production_capacity_count']; $i++) {
        $current_production_capacity_form = $production_capacity_form;
        $current_production_capacity_form['product_subfamilies']['#default_value'] = isset($values['production_capacity'][$i]['product_subfamilies']) ? $values['production_capacity'][$i]['product_subfamilies'] : '';
        $current_production_capacity_form['manufacturer_share']['#default_value'] = isset($values['production_capacity'][$i]['manufacturer_share']) ? $values['production_capacity'][$i]['manufacturer_share'] : '';
        $current_production_capacity_form['trader_share']['#default_value'] = isset($values['production_capacity'][$i]['trader_share']) ? $values['production_capacity'][$i]['trader_share'] : '';
        $current_production_capacity_form['agent_share']['#default_value'] = isset($values['production_capacity'][$i]['agent_share']) ? $values['production_capacity'][$i]['agent_share'] : '';
        $current_production_capacity_form['owned_production_unit_annual']['#default_value'] = isset($values['production_capacity'][$i]['owned_production_unit_annual']) ? $values['production_capacity'][$i]['owned_production_unit_annual'] : '';
        $current_production_capacity_form['owned_production_unit_annual_measure']['#default_value'] = isset($values['production_capacity'][$i]['owned_production_unit_annual_measure']) ? $values['production_capacity'][$i]['owned_production_unit_annual_measure'] : '';
        $current_production_capacity_form['owned_production_unit_last_year']['#default_value'] = isset($values['production_capacity'][$i]['owned_production_unit_last_year']) ? $values['production_capacity'][$i]['owned_production_unit_last_year'] : '';
        $current_production_capacity_form['owned_production_unit_last_year_measure']['#default_value'] = isset($values['production_capacity'][$i]['owned_production_unit_last_year_measure']) ? $values['production_capacity'][$i]['owned_production_unit_last_year_measure'] : '';
        $current_production_capacity_form['owned_production_unit_last_year_trading_only']['#default_value'] = isset($values['production_capacity'][$i]['owned_production_unit_last_year_trading_only']) ? $values['production_capacity'][$i]['owned_production_unit_last_year_trading_only'] : '';
        $current_production_capacity_form['owned_production_unit_last_year_trading_only_measure']['#default_value'] = isset($values['production_capacity'][$i]['owned_production_unit_last_year_trading_only_measure']) ? $values['production_capacity'][$i]['owned_production_unit_last_year_trading_only_measure'] : '';
        $form['fieldset_2']['production_capacity'][] = $current_production_capacity_form;
    }

    $storage_capacity_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'warehouse_factory_name' => array(
            '#type' => 'textfield',
            '#title' => 'Warehouse / Factory name',
            '#description' => 'Indicate the name of the warehouse / factory.',
        ),
        'location' => array(
            '#type' => 'select',
            '#title' => 'Country',
            '#description' => 'Location of the storage unit. The countries on the top of the list correspond to our allies\' markets.',
            '#empty_value' => '',
            '#options' => country_get_list(),
        ),
        'storage_capacity' => array(
            '#type' => 'textfield',
            '#title' => 'Storage capacity',
        ),
        'storage_capacity_measure' => array(
            '#type' => 'select',
            '#title' => 'Storage capacity measure',
            '#empty_value' => '',
            '#options' => $units_of_measure,
        ),
    );
    $form['fieldset_3'] = array(
        '#type' => 'fieldset',
        '#title' => 'Storage capacity',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $form['fieldset_3']['storage_capacity'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="storage-capacity">',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_3']['add_storage_capacity'] = array(
        '#type' => 'button',
        '#value' => 'Add storage capacity',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_production_information_form_ajax_add_storage_capacity',
            'wrapper' => 'storage-capacity',
        ),
    );
    $form['fieldset_3']['remove_storage_capacity'] = array(
        '#type' => 'button',
        '#value' => 'Remove storage capacity',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_production_information_form_ajax_remove_storage_capacity',
            'wrapper' => 'storage-capacity',
        ),
    );
    for ($i = 0; $i < $form_state['storage']['storage_capacity_count']; $i++) {
        $current_storage_capacity_form = $storage_capacity_form;
        $current_storage_capacity_form['warehouse_factory_name']['#default_value'] = isset($values['storage_capacity'][$i]['warehouse_factory_name']) ? $values['storage_capacity'][$i]['warehouse_factory_name'] : '';
        $current_storage_capacity_form['location']['#default_value'] = isset($values['storage_capacity'][$i]['location']) ? $values['storage_capacity'][$i]['location'] : '';
        $current_storage_capacity_form['storage_capacity']['#default_value'] = isset($values['storage_capacity'][$i]['storage_capacity']) ? $values['storage_capacity'][$i]['storage_capacity'] : '';
        $current_storage_capacity_form['storage_capacity_measure']['#default_value'] = isset($values['storage_capacity'][$i]['storage_capacity_measure']) ? $values['storage_capacity'][$i]['storage_capacity_measure'] : '';
        $form['fieldset_3']['storage_capacity'][] = $current_storage_capacity_form;
    }

    $form['fieldset_4'] = array(
        '#type' => 'fieldset',
        '#title' => 'Main production site overview',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        'production_sites_count' => array(
            '#type' => 'textfield',
            '#title' => 'Number of production units owned',
          '#default_value' => isset($values['production_sites_count']) ? $values['production_sites_count'] : '',
        ),
        'surface' => array(
            '#type' => 'textfield',
            '#title' => 'Total owned production surface in m2',
          '#default_value' => isset($values['surface']) ? $values['surface'] : '',
        ),
        'own_units_production_part' => array(
            '#type' => 'textfield',
            '#title' => 'Part produced in units of global activity',
          '#default_value' => isset($values['own_units_production_part']) ? $values['own_units_production_part'] : '',
            '#description' => 'in %'
        ),
        'sub_contracted_production_part' => array(
            '#type' => 'textfield',
            '#title' => 'Sub-contracted production ',
          '#default_value' => isset($values['sub_contracted_production_part']) ? $values['sub_contracted_production_part'] : '',
            '#description' => 'in %'
        ),
    );

    $factory_detail_form = array(
        '#type' => 'container',
        '#tree' => TRUE,
        'factory_name' => array(
            '#type' => 'textfield',
            '#title' => 'Factory name',
        ),
        'country' => array(
            '#type' => 'select',
            '#title' => 'Country',
            '#empty_value' => '',
            '#empty_value' => '',
            '#options' => country_get_list(),
            '#description' => 'The countries on the top of the list correspond to our allies\' markets.',
        ),
        'city' => array(
            '#type' => 'textfield',
            '#title' => 'City',
        ),
        'own_factory_partnership' => array(
            '#type' => 'textfield',
            '#title' => 'Own factory / partnership',
        ),
        'product_options' => array(
            '#type' => 'select',
            '#title' => 'Main Product subfamily produced',
            '#empty_value' => '',
            '#options' => $product_subfamilies,
        ),
        'production_capacity' => array(
            '#type' => 'textfield',
            '#title' => 'Production capacity',
        ),
        'production_capacity_measure' => array(
            '#type' => 'select',
            '#title' => 'Production capacity measure',
            '#empty_value' => '',
            '#options' => $units_of_measure
        ),
        'production_occupancy' => array(
            '#type' => 'textfield',
            '#title' => 'Production occupancy',
            '#description' => 'in %'
        ),
        'production_lead_time' => array(
            '#type' => 'textfield',
            '#title' => 'Production lead time',
            '#description' => 'Average, in open days'
        ),
        'last_year_turnover' => array(
            '#type' => 'textfield',
            '#title' => 'Last year turnover',
        ),
    );
    $form['fieldset_5'] = array(
        '#type' => 'fieldset',
        '#title' => 'Factory Details',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $form['fieldset_5']['factory_detail'] = array(
        '#type' => 'container',
        '#tree' => TRUE,
        '#prefix' => '<div id="factory-detail">',
        '#suffix' => '</div>',
        '#attributes' => array(),
    );
    $form['fieldset_5']['add_factory_detail'] = array(
        '#type' => 'button',
        '#value' => 'Add factory detail',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_production_information_form_ajax_add_factory_detail',
            'wrapper' => 'factory-detail',
        ),
    );
    $form['fieldset_5']['remove_factory_detail'] = array(
        '#type' => 'button',
        '#value' => 'Remove factory_detail',
        '#href' => '',
        '#limit_validation_errors' => array(),
        '#ajax' => array(
            'callback' => '_main_supplier_production_information_form_ajax_remove_factory_detail',
            'wrapper' => 'factory-detail',
        ),
    );

    for ($i = 0; $i < $form_state['storage']['factory_detail_count']; $i++) {
        $current_factory_detail_form = $factory_detail_form;
        $current_factory_detail_form['factory_name']['#default_value'] = isset($values['factory_detail'][$i]['factory_name']) ? $values['factory_detail'][$i]['factory_name'] : '';
        $current_factory_detail_form['country']['#default_value'] = isset($values['factory_detail'][$i]['country']) ? $values['factory_detail'][$i]['country'] : '';
        $current_factory_detail_form['city']['#default_value'] = isset($values['factory_detail'][$i]['city']) ? $values['factory_detail'][$i]['city'] : '';
        $current_factory_detail_form['own_factory_partnership']['#default_value'] = isset($values['factory_detail'][$i]['own_factory_partnership']) ? $values['factory_detail'][$i]['own_factory_partnership'] : '';
        $current_factory_detail_form['product_options']['#default_value'] = isset($values['factory_detail'][$i]['product_options']) ? $values['factory_detail'][$i]['product_options'] : '';
        $current_factory_detail_form['production_capacity']['#default_value'] = isset($values['factory_detail'][$i]['production_capacity']) ? $values['factory_detail'][$i]['production_capacity'] : '';
        $current_factory_detail_form['production_capacity_measure']['#default_value'] = isset($values['factory_detail'][$i]['production_capacity_measure']) ? $values['factory_detail'][$i]['production_capacity_measure'] : '';
        $current_factory_detail_form['production_occupancy']['#default_value'] = isset($values['factory_detail'][$i]['production_occupancy']) ? $values['factory_detail'][$i]['production_occupancy'] : '';
        $current_factory_detail_form['production_lead_time']['#default_value'] = isset($values['factory_detail'][$i]['production_lead_time']) ? $values['factory_detail'][$i]['production_lead_time'] : '';
        $current_factory_detail_form['last_year_turnover']['#default_value'] = isset($values['factory_detail'][$i]['last_year_turnover']) ? $values['factory_detail'][$i]['last_year_turnover'] : '';
        $form['fieldset_5']['factory_detail'][] = $current_factory_detail_form;
    }

    return _main_supplier_formalize($form);
}


function _main_supplier_production_information_form_validate($form, &$form_state) {
    $values = $form_state['values'];

    // Human resources
    foreach ($values['human_resource'] as $id => $human_resource) {
        foreach (array('employees', 'management') as $field_name) {
            $value = $human_resource[$field_name];
            if (!empty($value) && !_main_supplier_validate_integer_positive($value)) {
                form_set_error('human_resource]['.$id.'][' . $field_name, t('Invalid "@fieldname" value for "@groupname", you need to give a integer.',
                  array(
                    '@fieldname' => $form['fieldset_1']['human_resource'][$id][$field_name]['#title'],
                    '@groupname' => $form['fieldset_1']['#title'],
                  )
                ));
            }
        }
    }

    // Production capacity
    foreach ($values['production_capacity'] as $id => $production_capacity) {
        
      $fields_infos = array(
            array('name' => 'manufacturer_share', 'type' => 'number'),
            array('name' => 'trader_share', 'type' => 'number'),
            array('name' => 'agent_share', 'type' => 'number'),
            array('name' => 'owned_production_unit_annual', 'type' => 'integer'),
            array('name' => 'owned_production_unit_last_year', 'type' => 'integer'),
            array('name' => 'owned_production_unit_last_year_trading_only', 'type' => 'integer'),
        );
        
        foreach ($fields_infos as $field_infos) {

            $value = $production_capacity[$field_infos['name']];
            if (!empty($value) && !_main_supplier_validate_numeric_positive($value, $field_infos['type'])) {
                form_set_error('production_capacity]['.$id.'][' . $field_infos['name'], t('Invalid "@fieldname" value for @groupname, you need to give a @type.',
                  array(
                    '@fieldname' => $form['fieldset_2']['production_capacity'][$id][$field_infos['name']]['#title'],
                    '@groupname' => $form['fieldset_2']['#title'],
                    '@type' => $field_infos['type'],
                  )
                ));
            }
        }
    }

    // Storage capacity
    foreach ($values['storage_capacity'] as $id => $production_capacity) {
        foreach (array('storage_capacity') as $field_name) {
            $value = $production_capacity[$field_name];
            if (!empty($value) && !_main_supplier_validate_integer_positive($value)) {
                form_set_error('storage_capacity]['.$id.'][' . $field_name, t('Invalid "@fieldname" value for "@groupname", you need to give a integer.',
                  array(
                    '@fieldname' => $form['fieldset_3']['storage_capacity'][$id][$field_name]['#title'],
                    '@groupname' => $form['fieldset_3']['#title'],
                  )
                ));
            }
        }
    }

    // Main production site overview
    
    $fields_infos = array(
        array('name' => 'production_sites_count', 'type' => 'integer'),
        array('name' => 'surface', 'type' => 'integer'),
        array('name' => 'own_units_production_part', 'type' => 'number'),
        array('name' => 'sub_contracted_production_part', 'type' => 'number'),
    );
    
    foreach ($fields_infos as $field_infos) {

        $value = $values[$field_infos['name']];
        if (!empty($value) && !_main_supplier_validate_numeric_positive($value, $field_infos['type'])) {
            form_set_error($field_infos['name'], t('Invalid "@fieldname" value for "@groupname", you need to give a @type.',
              array(
                '@fieldname' => $form['fieldset_4'][$field_infos['name']]['#title'],
                '@groupname' => $form['fieldset_4']['#title'],
                '@type' => $field_infos['type'],
              )
            ));
        }
    }

    // Factory Details
    
    $fields_infos = array(
        array('name' => 'production_occupancy', 'type' => 'number'), 
        array('name' => 'production_capacity', 'type' => 'integer'),
        array('name' => 'production_lead_time', 'type' => 'integer'),
        array('name' => 'last_year_turnover', 'type' => 'integer'),
    );
    
    foreach ($values['factory_detail'] as $id => $production_capacity) {
        foreach ($fields_infos as $field_infos) {

            $value = $production_capacity[$field_infos['name']];
            if (!empty($value) && !_main_supplier_validate_numeric_positive($value, $field_infos['type'])) {
                form_set_error('factory_detail]['.$id.'][' . $field_infos['name'], t('Invalid "@fieldname" value for "@groupname", you need to give a @type.',
                  array(
                    '@fieldname' => $form['fieldset_5']['factory_detail'][$id][$field_infos['name']]['#title'],
                    '@groupname' => $form['fieldset_5']['#title'],
                    '@type' => $field_infos['type'],
                  )
                ));
            }
        }
    }
}

function _main_supplier_production_information_form_submit($form, &$form_state) {
    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);
    $w_profile = entity_metadata_wrapper('node', $profile);
    $values = $form_state['values'];

    foreach (array('submit', 'form_build_id', 'form_token', 'form_id', 'op', 'add_storage_capacity',
               'remove_storage_capacity', 'add_production_capacity', 'remove_production_capacity', 'add_human_resource',
               'remove_human_resource', 'add_factory_detail', 'remove_factory_detail') as $value) {
        unset($values[$value]);
    }

    $w_profile->field_production_information = json_encode($values);
    $w_profile->save();

    drupal_set_message('Information has been saved.');
    drupal_goto('supplier/quality-control');
}

/**********
 ** AJAX **
 **********/
function _main_supplier_production_information_form_ajax_add_human_resource($form, &$form_state) {
    return $form['fieldset_1']['human_resource'];
}

function _main_supplier_production_information_form_ajax_remove_human_resource($form, &$form_state) {
    return $form['fieldset_1']['human_resource'];
}

function _main_supplier_production_information_form_ajax_add_production_capacity($form, &$form_state) {
    return $form['fieldset_2']['production_capacity'];
}

function _main_supplier_production_information_form_ajax_remove_production_capacity($form, &$form_state) {
    return $form['fieldset_2']['production_capacity'];
}

function _main_supplier_production_information_form_ajax_add_storage_capacity($form, &$form_state) {
    return $form['fieldset_3']['storage_capacity'];
}

function _main_supplier_production_information_form_ajax_remove_storage_capacity($form, &$form_state) {
    return $form['fieldset_3']['storage_capacity'];
}

function _main_supplier_production_information_form_ajax_add_factory_detail($form, &$form_state) {
    return $form['fieldset_5']['factory_detail'];
}

function _main_supplier_production_information_form_ajax_remove_factory_detail($form, &$form_state) {
    return $form['fieldset_5']['factory_detail'];
}
