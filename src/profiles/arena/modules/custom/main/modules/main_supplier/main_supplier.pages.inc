<?php

function _main_supplier()
{
    drupal_goto('supplier/user-information');
}

function _main_supplier_alias($node) {
    drupal_set_title($node->title);
    return node_view($node);
}

function _main_supplier_user_information()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    $renderable['right'][] = drupal_get_form('_main_supplier_user_information_form');
    return $renderable;
}

function _main_supplier_description()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    if (MainSupplierAPI::getCurrentUid() > 0) {
        $renderable['right'][] = drupal_get_form('_main_supplier_description_form');
    } else {
        $renderable['right'][] = array(
            '#markup' => 'Please fill in the User information tab before.'
        );
    }
    return $renderable;
}

function _main_supplier_general_information()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    if (MainSupplierAPI::getCurrentNid() > 0) {
        $renderable['right'][] = drupal_get_form('_main_supplier_general_information_form');
    } else {
        $renderable['right'][] = array(
            '#markup' => 'Please fill in the Description tab before.'
        );
    }
    return $renderable;
}

function _main_supplier_additional_information()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    $renderable['right'][] = drupal_get_form('_main_supplier_additional_information_form');
    return $renderable;
}

function _main_supplier_market_client_information()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    $renderable['right'][] = drupal_get_form('_main_supplier_market_client_information_form');
    return $renderable;
}

function _main_supplier_production_information()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    $renderable['right'][] = drupal_get_form('_main_supplier_production_information_form');
    return $renderable;
}

function _main_supplier_quality_control()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    $renderable['right'][] = drupal_get_form('_main_supplier_quality_control_form');
    return $renderable;
}

function _main_supplier_media()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    $renderable['right'][] = drupal_get_form('_main_supplier_media_form');
    return $renderable;
}

function _main_supplier_communication_documents()
{
    if (!MainSupplierAPI::isDeviceGranted()) {
        return _main_supplier_device_not_granted();
    }

    $renderable = _main_supplier_get_template();
    $renderable['right'][] = drupal_get_form('_main_supplier_document_form');
    return $renderable;
}

//utils
function _main_supplier_get_template()
{
    $renderable = array();
    $renderable['left'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-bar')),
        'summary' => array(
            '#type' => 'fieldset',
            '#title' => 'Summary',
            '#attributes' => array('class' => array('supplier-tabs')),
            'tabs' => array(
                _main_supplier_get_tabs(),
            ),
        ),
    );

    $nid = MainSupplierAPI::getCurrentNid();
    if (!MainSupplierAPI::isProfileValidated($nid)) {
        $renderable['left']['summary_submit'] = array(
            drupal_get_form('_main_supplier_submit_for_validation_form')
        );
    }

    $renderable['right'] = array(
        '#type' => 'fieldset',
        '#attributes' => array('class' => array('supplier-form')),
    );

    return $renderable;
}

function _main_supplier_get_tabs()
{
    global $user;

    $tabs = array();

    $step1_valid = MainSupplierAPI::isStepValid(1);
    $tabs[] = array(
        '#theme' => 'link',
        '#text' => 'User information',
        '#path' => 'supplier/user-information',
        '#options' => array(
            'attributes' => array('class' => $step1_valid ? array('complete') : ''),
            'html' => false,
        ),
    );

    $step2_valid = MainSupplierAPI::isStepValid(2);
    $tabs[] = array(
        '#theme' => 'link',
        '#text' => 'Description',
        '#path' => 'supplier/description',
        '#options' => array(
            'attributes' => array('class' => $step2_valid ? array('complete') : ''),
            'html' => false,
        ),
    );

    $step3_valid = MainSupplierAPI::isStepValid(3);
    $tabs[] = array(
        '#theme' => 'link',
        '#text' => 'General information',
        '#path' => 'supplier/general-information',
        '#options' => array(
            'attributes' => array('class' => $step3_valid ? array('complete') : ''),
            'html' => false,
        ),
    );

    $nid = MainSupplierAPI::getCurrentNid();
    if (MainSupplierAPI::isProfileValidated($nid)) {
        $step4_valid = MainSupplierAPI::isStepValid(4);
        $tabs[] = array(
            '#theme' => 'link',
            '#text' => 'Additional information',
            '#path' => 'supplier/additional-information',
            '#options' => array(
                'attributes' => array('class' => $step4_valid ? array('complete') : ''),
                'html' => false,
            ),
        );

        $step5_valid = MainSupplierAPI::isStepValid(5);
        $tabs[] = array(
            '#theme' => 'link',
            '#text' => 'Market / Client information',
            '#path' => 'supplier/market-client-information',
            '#options' => array(
                'attributes' => array('class' => $step5_valid ? array('complete') : ''),
                'html' => false,
            ),
        );

        $step6_valid = MainSupplierAPI::isStepValid(6);
        $tabs[] = array(
            '#theme' => 'link',
            '#text' => 'Production information',
            '#path' => 'supplier/production-information',
            '#options' => array(
                'attributes' => array('class' => $step6_valid ? array('complete') : ''),
                'html' => false,
            ),
        );

        $step7_valid = MainSupplierAPI::isStepValid(7);
        $tabs[] = array(
            '#theme' => 'link',
            '#text' => 'Quality control',
            '#path' => 'supplier/quality-control',
            '#options' => array(
                'attributes' => array('class' => $step7_valid ? array('complete') : ''),
                'html' => false,
            ),
        );

//        $step8_valid = MainSupplierAPI::isStepValid(8);
//        $tabs[] = array(
//            '#theme' => 'link',
//            '#text' => 'Media',
//            '#path' => 'supplier/media',
//            '#options' => array(
//                'attributes' => array('class' => $step8_valid ? array('complete') : ''),
//                'html' => false,
//            ),
//        );
//
//        $step9_valid = MainSupplierAPI::isStepValid(9);
//        $tabs[] = array(
//            '#theme' => 'link',
//            '#text' => 'Communication documents',
//            '#path' => 'supplier/communication-documents',
//            '#options' => array(
//                'attributes' => array('class' => $step9_valid ? array('complete') : ''),
//                'html' => false,
//            ),
//        );
    }

    return $tabs;
}

function _main_supplier_device_not_granted() {
    return 'This device does not have access to this page. Please try from a desktop computer.';
}
