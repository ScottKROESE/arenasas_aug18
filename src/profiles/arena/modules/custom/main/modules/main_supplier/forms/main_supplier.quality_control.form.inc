<?php

function _main_supplier_quality_control_form($form, &$form_state) {

    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);

    $values = isset($profile->field_quality_control[LANGUAGE_NONE][0]['value']) ? $profile->field_quality_control[LANGUAGE_NONE][0]['value'] : NULL;
    if ($values) {
        $values = json_decode($values, TRUE);
    }

    $form['process_certification'] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => 'Process certification',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        'elements' => array(
            '#type' => 'checkboxes',
            '#options' => array(
                'iso_9001' => 'ISO 9001',
                'iso_14000' => 'ISO 14000',
                'bsci' => 'BSCI',
                'sa8000' => 'SA8000',
                'fsc' => 'FSC',
                'pefc' => 'PEFC',
                'other' => 'Other'
            ),
            '#default_value' => isset($values['process_certification']['elements']) ? $values['process_certification']['elements'] : array(),
        ),
        'please_precise' => array(
            '#type' => 'textfield',
            '#title' => 'Please precise',
            '#default_value' => isset($values['process_certification']['please_precise']) ? $values['process_certification']['please_precise'] : '',
            '#states' => array('visible' => array(
              'input[id="edit-process-certification-elements-other"]' => array('checked' => TRUE),
            )),
        ),
    );

    $form['product_certificate'] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => 'Product certificate',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        'elements' => array(
            '#type' => 'checkboxes',
            '#options' => array(
                'ce' => 'CE',
                'ece' => 'ECE',
                'voc' => 'VOC',
                'erp' => 'ERP',
                'gost' => 'GOST',
                'tisi' => 'TISI',
                'other' => 'Other'
            ),
            '#default_value' => isset($values['product_certificate']['elements']) ? $values['product_certificate']['elements'] : array(),

        ),
        'please_precise' => array(
            '#type' => 'textfield',
            '#title' => 'Please precise',
            '#default_value' => isset($values['product_certificate']['please_precise']) ? $values['product_certificate']['please_precise'] : '',

            '#states' => array('visible' => array(
                'input[id="edit-product-certificate-elements-other"]' => array('checked' => TRUE),
            )),
        ),
    );

    $form['quality_label'] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => 'Quality label',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
        'elements' => array(
            '#type' => 'checkboxes',
            '#options' => array(
                'gs' => 'GS',
                'nf' => 'NF',
                'vde' => 'VDE',
                'ecolabel' => 'Ecolabel',
                'other' => 'Other'
            ),
            '#default_value' => isset($values['quality_label']['elements']) ? $values['quality_label']['elements'] : array(),

        ),
        'please_precise' => array(
            '#type' => 'textfield',
            '#title' => 'Please precise',
            '#default_value' => isset($values['quality_label']['please_precise']) ? $values['quality_label']['please_precise'] : '',
            '#states' => array('visible' => array(
                'input[id="edit-quality-label-elements-other"]' => array('checked' => TRUE),
            )),
        )
    );

    $quality_indicator_elements = array(
        'own_plants' => array(
            'Incoming goods control conformity',
            'Control conformity during manufacturing process',
            'Control conformity at the end of the manufacturing process',
            'Service Rate',
            'Warranty Failure',
        ),
        'trading_activities' => array(
            'Incoming goods control conformity',
            'Control conformity during manufacturing process'
        ),
    );
    $form['quality_indicator'] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => 'Quality indicator you follow',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $form['quality_indicator']['own_plants'] = array(
        '#type' => 'container',
        '#prefix' => '<span class="panel-subtitle">Own plants</span>'
    );
    $form['quality_indicator']['trading_activities'] = array(
        '#type' => 'container',
        '#prefix' => '<span class="panel-subtitle">Trading activities</span>'
    );
    foreach ($quality_indicator_elements['own_plants'] as $key => $element) {
        $machine_name = preg_replace('/[^a-z0-9_]+/', '_', strtolower($element));
        $form['quality_indicator']['own_plants'][$machine_name] = array(
            'check' => array(
                '#type' => 'checkbox',
                '#title' => $element,
                '#default_value' => isset($values['quality_indicator']['own_plants'][$machine_name]['check']) ?
                  $values['quality_indicator']['own_plants'][$machine_name]['check'] : '',
            ),
            'additional_information' => array(
                '#type' => 'textarea',
                '#title' => 'Explanations / additional information',
                '#default_value' => isset($values['quality_indicator']['own_plants'][$machine_name]['additional_information']) ?
                  $values['quality_indicator']['own_plants'][$machine_name]['additional_information'] : '',
                '#states' => array('visible' => array(
                    'input[name="quality_indicator[own_plants]['.$machine_name.'][check]"]' => array('checked' => TRUE),
                )),
            )
        );
    }
    foreach ($quality_indicator_elements['trading_activities'] as $key => $element) {
        $machine_name = preg_replace('/[^a-z0-9_]+/', '_', strtolower($element));
        $form['quality_indicator']['trading_activities'][$machine_name] = array(
            'check' => array(
                '#type' => 'checkbox',
                '#title' => $element,
                '#default_value' => isset($values['quality_indicator']['trading_activities'][$machine_name]['check']) ?
                  $values['quality_indicator']['trading_activities'][$machine_name]['check'] : '',
            ),
            'additional_information' => array(
                '#type' => 'textarea',
                '#title' => 'Additional information',
                '#default_value' => isset($values['quality_indicator']['trading_activities'][$machine_name]['additional_information']) ?
                  $values['quality_indicator']['trading_activities'][$machine_name]['additional_information'] : '',
                '#states' => array('visible' => array(
                    'input[name="quality_indicator[trading_activities]['.$machine_name.'][check]"]' => array('checked' => TRUE),
                )),
            )
        );
    }

    $product_development_and_qualification_elements = array(
        'own_plants' => array(
            'Formal product Functional Specifications',
            'Formalized Product Qualification process',
            'Formalized Product Qualification report',
            'Product Life Test',
            'Product FMEA during product development',
            'Process FMEA during production',
            'Product Life Test'
        ),
        'trading_activities' => array(
            'Supplier qualification audit',
            'Product qualification by the supplier',
            'Product qualification by the service',
            'Supplier process audit',
            'Do you have formalized Supplier Quality manual ?'
        ),
    );
    $form['product_development_and_qualification'] = array(
        '#type' => 'fieldset',
        '#tree' => TRUE,
        '#title' => 'Product development and qualification',
        '#attributes' => array('class' => array('supplier-form-fieldset')),
    );
    $form['product_development_and_qualification']['own_plants'] = array(
        '#type' => 'container',
        '#prefix' => '<span class="panel-subtitle">Own plants</span>'
    );
    $form['product_development_and_qualification']['trading_activities'] = array(
        '#type' => 'container',
        '#prefix' => '<span class="panel-subtitle">Trading activities</span>'
    );
    foreach ($product_development_and_qualification_elements['own_plants'] as $key => $element) {
        $machine_name = preg_replace('/[^a-z0-9_]+/', '_', strtolower($element));
        $form['product_development_and_qualification']['own_plants'][$machine_name] = array(
            'check' => array(
                '#type' => 'checkbox',
                '#title' => $element,
                '#default_value' => isset($values['product_development_and_qualification']['own_plants'][$machine_name]['check']) ?
                  $values['product_development_and_qualification']['own_plants'][$machine_name]['check'] : '',
            ),
            'additional_information' => array(
                '#type' => 'textarea',
                '#title' => 'Additional information',
                '#default_value' => isset($values['product_development_and_qualification']['own_plants'][$machine_name]['additional_information']) ?
                  $values['product_development_and_qualification']['own_plants'][$machine_name]['additional_information'] : '',
                '#states' => array('visible' => array(
                    'input[name="product_development_and_qualification[own_plants]['.$machine_name.'][check]"]' => array('checked' => TRUE),
                )),
            )
        );
    }
    foreach ($product_development_and_qualification_elements['trading_activities'] as $key => $element) {
        $machine_name = preg_replace('/[^a-z0-9_]+/', '_', strtolower($element));
        $form['product_development_and_qualification']['trading_activities'][$machine_name] = array(
            'check' => array(
                '#type' => 'checkbox',
                '#title' => $element,
                '#default_value' => isset($values['product_development_and_qualification']['trading_activities'][$machine_name]['check']) ?
                  $values['product_development_and_qualification']['trading_activities'][$machine_name]['check'] : '',
            ),
            'additional_information' => array(
                '#type' => 'textarea',
                '#title' => 'Additional information',
                '#default_value' => isset($values['product_development_and_qualification']['trading_activities'][$machine_name]['additional_information']) ?
                  $values['product_development_and_qualification']['trading_activities'][$machine_name]['additional_information'] : '',
                '#states' => array('visible' => array(
                  'input[name="product_development_and_qualification[trading_activities]['.$machine_name.'][check]"]' => array('checked' => TRUE),
                )),
            )
        );
    }

    return _main_supplier_formalize($form);
}

/**
 * Validate.
 */
function _main_supplier_quality_control_form_validate($form, &$form_state) {
    if (!empty($form_state['values']['process_certification']['elements']['other']) && empty($form_state['values']['process_certification']['please_precise'])) {
        form_set_error('process_certification][please_precise', 'If you check other, you must precise.');
    }
    if (!empty($form_state['values']['product_certificate']['elements']['other']) && empty($form_state['values']['product_certificate']['please_precise'])) {
        form_set_error('product_certificate][please_precise', 'If you check other, you must precise.');
    }
    if (!empty($form_state['values']['quality_label']['elements']['other']) && empty($form_state['values']['quality_label']['please_precise'])) {
        form_set_error('quality_label][please_precise', 'If you check other, you must precise.');
    }

    foreach($form_state['values']['quality_indicator']['own_plants'] as $key => $value) {
        if (!empty($value['check']) && empty($value['additional_information'])) {
            form_set_error($key, 'Additional information must be filled when the checkbox is checked.');
        }
    }

    foreach($form_state['values']['quality_indicator']['trading_activities'] as $key => $value) {
        if (!empty($value['check']) && empty($value['additional_information'])) {
            form_set_error($key, 'Additional information must be filled when the checkbox is checked.');
        }
    }

    foreach($form_state['values']['product_development_and_qualification']['own_plants'] as $key => $value) {
        if (!empty($value['check']) && empty($value['additional_information'])) {
            form_set_error($key, 'Additional information must be filled when the checkbox is checked.');
        }
    }

    foreach($form_state['values']['product_development_and_qualification']['trading_activities'] as $key => $value) {
        if (!empty($value['check']) && empty($value['additional_information'])) {
            form_set_error($key, 'Additional information must be filled when the checkbox is checked.');
        }
    }
}

/**
 * Callback form submit.
 */
function _main_supplier_quality_control_form_submit($form, &$form_state) {
    $nid = MainSupplierAPI::getCurrentNid();
    $profile = node_load($nid);
    $w_profile = entity_metadata_wrapper('node', $profile);
    $values = $form_state['values'];

    foreach (array('submit', 'form_build_id', 'form_token', 'form_id', 'op') as $value) {
        unset($values[$value]);
    }

    $data = $values;

    $w_profile->field_quality_control = json_encode($data);
    $w_profile->save();

    drupal_set_message('Information has been saved.');

    $path = drupal_get_path_alias('node/' . $nid);
    drupal_goto($path);
}
