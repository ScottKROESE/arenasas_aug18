<?php

/**
 * Implements hook_user_view().
 */
function main_user_user_view($account, $view_mode, $langcode)
{
    if (MainUser::hasRole('suppliers')) {
        $profile_nid = MainSupplierAPI::getUserProfileNid($account->uid);
        if ($profile_nid) {
            $path = drupal_get_path_alias('node/' . $profile_nid);
            drupal_goto($path);
        }
    }
    elseif (MainUser::hasRole('invite')) {
        drupal_goto('my-space');
    }
    elseif (MainUser::hasRole('internal')) {
        drupal_goto('my-space');
    }
    elseif (MainUser::hasRole('admin')) {
        drupal_goto('manage');
    } else {
        drupal_goto('');
    }
}

/**
 * Implements hook_menu_alter().
 */
function main_user_menu_alter(&$items) {
    $items['user']['access callback'] = 'user_is_logged_in';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function main_user_form_user_register_form_alter(&$form, $form_state, $form_id) {
    $form['locale']['language']['#default_value'] = 'en-en';
    $form['locale']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function main_user_form_user_pass_alter(&$form, $form_state, $form_id) {
  $form['#submit'][] = 'main_user_form_user_pass_submit';
}

/**
 * Form submit callback for user_pass form.
 * Redirect to user/login (default redirect to user).
 */
function main_user_form_user_pass_submit($form, &$form_state) {
  $form_state['redirect'] = 'user/login';
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function main_user_form_user_profile_form_alter(&$form, $form_state, $form_id) {
    $account = user_load($form['#user']->uid);
    $title = '';
    if (isset($account->field_first_name[LANGUAGE_NONE][0]['value'])) {
        $title .= $account->field_first_name[LANGUAGE_NONE][0]['value'];
        $title .= ' ';
    }
    if (isset($account->field_last_name[LANGUAGE_NONE][0]['value'])) {
        $title .= $account->field_last_name[LANGUAGE_NONE][0]['value'];
    }

    drupal_set_title($title);
    $form['locale']['language']['#default_value'] = 'en-en';
    $form['locale']['#access'] = FALSE;
    $form['contact']['#access'] = FALSE;
    if (!user_access('administer users')) {
        $form['account']['mail']['#access'] = FALSE;
    }

    $account = user_load($form['#user']->uid);
    if (!MainUser::hasRole('internal', $account)) {
        $form['field_company']['#access'] = FALSE;
    }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function main_user_form_user_login_alter(&$form, $form_state, $form_id)
{
    // Add forgotten password link for password input
    $languages = language_list();
    $form['pass']['#description'] = l(t('Forgotten your password ?'), 'user/password', array('language' => $languages['en-en']));
    $form['pass']['#description'] .= '<p><em>'.t('* required field.').'</em></p>';
}

/**
 * Implements hook_mail_alter().
 * Remove language prefix for link to reset password.
 * We can't use hook_tokens_alter(), the token [user:one-time-login-url] is not a real token,
 * see user_mail_tokens().
 */
function main_user_mail_alter(&$message) {
    // mail ids with one time login url
    $user_mails_with_on_time_login_url = array(
        'user_register_no_approval_required',
        'user_register_admin_created',
        'user_password_reset',
        'user_status_activated',
    );

    // Remove language prefix for one time login url
    if (in_array($message['id'], $user_mails_with_on_time_login_url)) {
        foreach ($message['body'] as $id => $body) {
            $message['body'][$id] = preg_replace('|/../user/reset/|', '/user/reset/', $body);
        }
    }
}
