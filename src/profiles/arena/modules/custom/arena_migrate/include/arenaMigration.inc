<?php

/**
 * This abstract class contains basic informations for classes
 */
abstract class arenaMigration extends Migration {

    const IMPORT_LANGUAGE = LANGUAGE_NONE;
    const IMPORT_UID = 1;


    /**
     * Construct method.
     */
    public function __construct($arguments) {
        parent::__construct($arguments);
        $this->team = array(
          new MigrateTeamMember('Quentin', 'quentin.labadens@buzzaka.com', t('Implementor')),
        );
    }
}