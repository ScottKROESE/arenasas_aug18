<?php
/**
 * @file
 * arena_content_type_process.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function arena_content_type_process_taxonomy_default_vocabularies() {
  return array(
    'document_category' => array(
      'name' => 'Document category',
      'machine_name' => 'document_category',
      'description' => '',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
      'language' => 'und',
      'i18n_mode' => 0,
    ),
  );
}
