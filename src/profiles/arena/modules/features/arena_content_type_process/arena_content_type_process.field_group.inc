<?php
/**
 * @file
 * arena_content_type_process.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function arena_content_type_process_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_call_to_action|node|process|form';
  $field_group->group_name = 'group_call_to_action';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'process';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Call to actions',
    'weight' => '5',
    'children' => array(
      0 => 'field_call_to_action_1',
      1 => 'field_call_to_action_1_text_link',
      2 => 'field_call_to_action_2',
      3 => 'field_call_to_action_2_text_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Call to actions',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-call-to-action field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_call_to_action|node|process|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Call to actions');

  return $field_groups;
}
