<?php

/**
 * @file
 * Search API data alteration callback.
 */

class SearchApiExtraFieldAlterSettings extends SearchApiAbstractAlterCallback {

    /**
     * {@inheritdoc}
     */
    public function alterItems(array &$items) {
        module_load_include('inc', 'arena_search_api', 'includes/countries_zones');
        $countries_zones = _arena_country_get_zone_list();

        foreach ($items as $id => &$item) {
            if ($item->type == 'supplier_profile') {
                $wnode = entity_metadata_wrapper('node', $item);
                $country_code = $wnode->field_country->value();
                if (isset($countries_zones[$country_code])) {
                    $item->field_extra_zone_arena = $countries_zones[$country_code]['zone'];
                }
                else {
                    $item->field_extra_zone_arena = 'Unknown';
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function propertyInfo() {
        $ret = array();

        $title = 'Zone (ARENA)';
        $ret['field_extra_zone_arena'] = array(
          'label' => 'Extra field : '.$title,
          'description' => $title,
          'type' => 'text',
        );

        return $ret;
    }

}
