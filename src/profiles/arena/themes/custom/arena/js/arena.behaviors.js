(function ($) {

    Drupal.behaviors.dropdownMenu = {
        attach: function () {

        }
    };

//    Drupal.behaviors.stickyAside = {
//        attach: function (context, settings) {
//            $(window).on('resize', function () {
//                var win = $(this); //this = window
//                if (win.width() >= 992) {
//                    var offset = $("#block-main-private-main-private-contextual").offset();
//                    var topPadding = 40;
//                    if (typeof offset !== 'undefined') {
//                        $(window).scroll(function () {
//                            if ($(window).scrollTop() > offset.top) {
//                                $("#block-main-private-main-private-contextual").css({
//                                    marginTop: $(window).scrollTop() - offset.top + topPadding
//                                });
//                            } else {
//                                $("#block-main-private-main-private-contextual").css({
//                                    marginTop: 0
//                                });
//                            }
//                        });
//                    }
//                }
//            })
//        }
//    };

    Drupal.behaviors.stickyAsideGeneric = {
        attach: function (context, settings) {
            var $sidebar   = $("#block-main-private-main-private-contextual"),
            $window    = $(window),
            offset     = $sidebar.offset(),
            topPadding = 15;

            if ( $sidebar.length ) {
                $window.scroll(function() {
                    if ($window.width() >= 993) {
                        if ($window.scrollTop() > offset.top) {
                        $sidebar.stop().animate({
                            marginTop: $window.scrollTop() - offset.top + topPadding,
                            paddingTop: 30
                        }, 300);
                        } else {
                            $sidebar.stop().animate({
                                marginTop: 30,
                                paddingTop: 0
                            });
                        }
                    } else {
                        $sidebar.css({marginTop: 30, paddingTop: 0});
                    }
                });
            }
        }
    };

    Drupal.behaviors.animatedKeyFigures = {
        attach: function (context, settings) {
            $('.field-name-field-figure .field-items .field-item').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        }
    };

    /**
     * Open/close language switcher.
     */
    Drupal.behaviors.arenaLanguageSwitcher = {
        attach: function (context, settings) {
            $('#block-locale-language .current-lang').click(
                function () {
                    $('#block-locale-language').toggleClass('open');
                }
            );
            // Set cookie when language is change.
            $('.language-switcher-locale-url li a').click(
                function () {
                    var lang = $(this).attr('xml:lang');
                    $.cookie("Drupal.visitor.arenaLang", lang, { expires : 365, path: '/' });
                }
            );
        }
    };

    /**
     * Auto submit sort option for product catalog
     * @type {{attach: Function}}
     */
    Drupal.behaviors.arenaProductCatalogueSort = {
        attach: function (context, settings) {
            if ($('#block-arena-search-api-expose-filter-sort-by').length) {
                $('#block-arena-search-api-expose-filter-sort-by input[type=radio]').change(function () {
                    $('#block-arena-search-api-expose-filter-sort-by button.form-submit').click();
                });
                $('#block-arena-search-api-expose-filter-sort-by input:checked').parent().addClass('active');
            }
        }
    };

    Drupal.behaviors.submitObserver = {
        attach: function (context, settings) {
            $('button#edit-submit').on('click', function (e) {
                if ($(this).hasClass('disabled')) e.preventDefault();
            });
        }
    };

    Drupal.behaviors.arenaProductCatalogFilter = {
        attach: function (context, settings) {
            $('.block-facetapi').each(function (index, element) {
                var $element = $(element);
                $element.find('h2').click(function (event) {
                    $('.block-facetapi.active').each(function (i, e) {
                        var $e = $(e);
                        if (!$e.is($element)) $e.removeClass('active');
                    });
                    $element.toggleClass('active');
                });
            });
        }
    };

    Drupal.behaviors.arenaDirectorySuppliersFilter = {
        attach: function (context, settings) {
            $('#block-arena-search-api-suppliers-expose-filter-sort-by div div.item-list h3').on('click', function (e) {
                $(this).parents('.item-list').toggleClass('on');
            });
        }
    };

    Drupal.behaviors.toggleGroupSupplierInfos = {
        attach: function (context, settings) {
            $('.suppliers-infos-details .field-label').on('click', function (e) {
                e.preventDefault();
                var that = $(this).parents('.suppliers-infos-details');
                $('.suppliers-infos-details').each(function (i) {
                    if (!that.is($(this))) {
                        $(this).removeClass('open');
                    }
                });
                $(this).parents('.suppliers-infos-details').hasClass('open') ? $(this).parents('.suppliers-infos-details').removeClass('open') : $(this).parents('.suppliers-infos-details').addClass('open');
            });
        }
    };

    Drupal.behaviors.burgerObserver = {
        attach: function (context, settings) {
            $('div.burger-icon').on('click', function (e) {
                $(this).toggleClass('cross');
                $('.region-header').toggleClass('on');
            });
        }
    };

    Drupal.behaviors.observer = {
        attach: function (context, settings) {

            if ($('body.node-type-page-group').length > 0) {
                $('.form-item[class*="-measure"]').addClass('measure-type');
                $('.form-item[class*="-measure"]').prev().addClass('unit-type');

                // Collapse chronology
                $('.group-chronology a.expander-button').on('click', function (e) {
                    e.preventDefault();
                    console.log('toto');
                    $currentTarget = $(e.currentTarget);
                    $slider = $('.group-chronology .field-name-field-chronology-items-group > .field-items');
                    if ($slider.hasClass('on')) {
                        $currentTarget.text(Drupal.t('Read more'));
                    } else {
                        $currentTarget.text(Drupal.t('Read less'));
                    }
                    $slider.toggleClass('on');
                });
            }
        }
    }
})(jQuery);
