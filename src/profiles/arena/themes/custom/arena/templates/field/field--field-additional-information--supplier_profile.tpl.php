<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
?>
<div class="<?php print $classes; ?> suppliers-infos-details"<?php print $attributes; ?>>
    <?php if (!$label_hidden): ?>
        <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?></div>
    <?php endif; ?>
    <div class="field-items"<?php print $content_attributes; ?>>
        <?php foreach($items as $key => $value): ?>
            <div class="row field-item <?php print $key % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$key]; ?>>
                <?php
                    $form = drupal_get_form('_main_supplier_additional_information_form');
                    $value = json_decode($value['#markup'], TRUE);

                ?>
                <div class="table-wrapper col-md-6">
                    <!-- CAPITAL -->
                    <div class="table-title">Capital</div>
                    <table>
                        <tr>
                            <td>
                                <?php echo $form['fieldset_2']['#title']; ?>
                            </td>
                            <td>
                                <?php
                                    echo $value['capital'] .'&nbsp;';
                                    echo $value['capital_currency'];
                                ?>
                            </td>
                        </tr>
                    </table>
                    <!-- SHAREHOLDERS -->
                    <?php if (!empty($value['shareholder'])): ?>
                    <div class="table-title">Shareholders</div>
                    <table class="top-header">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>% Share owned</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($value['shareholder'] as $key2 => $value2): ?>
                                <tr>
                                    <td>
                                        <?php echo $value2['shareholder_name']; ?>
                                    </td>
                                    <td>
                                        <?php echo $value2['shareholder_share']; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?php endif; ?>
                </div>
                <div class="table-wrapper col-md-6">
                    <!-- BRANDS -->
                    <?php if (!empty($value['brand'])): ?>
                        <div class="table-title">Owned brands</div>
                        <table>
                            <?php foreach($value['brand'] as $key2 => $value2): ?>
                                <tr>
                                    <td>Name</td>
                                    <td><?php echo $value2['brand_name']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                    <!-- LICENSE -->
                    <?php if (!empty($value['license'])): ?>
                        <div class="table-title">Held license</div>
                        <table>
                            <?php foreach($value['license'] as $key2 => $value2): ?>
                                <tr>
                                    <td>Name</td>
                                    <td><?php echo $value2['license_name']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                </div>
                <?php if (!empty($value['affiliate'])): ?>
                    <div class="table-wrapper col-md-12">
                        <!-- AFFILIATES -->
                        <div class="table-title top-header">List of Subsidiaries / Affiliates</div>
                        <table class="top-header">
                            <thead>
                                <tr>
                                    <td>Name</td>
                                    <td>Country</td>
                                    <td>Product Families</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $countries = country_get_list();
                                    foreach($value['affiliate'] as $key2 => $value2):
                                        echo '<tr>';
                                        echo '<td>'.$value2['company_name'].'</td>';
                                        if (!empty($value2['country'])) {
                                            echo '<td>' . $countries[$value2['country']] . '</td>';
                                        } else {
                                            echo '<td></td>';
                                        }
                                        echo '<td>';
                                        foreach($value2['product_families'] as $key3 => $value3):
                                            $str = '';
                                            foreach ($value3 as $key4 => $value4):
                                                if ($value4 !== 0) {
                                                    $term = taxonomy_term_load($value4);
                                                    $str .= $term->name . ', ';
                                                }
                                                // echo substr($str, 0, -2);
                                            endforeach;
                                            echo substr($str, 0, -2);
                                        endforeach;
                                        echo '</td>';
                                        echo '</tr>';
                                    endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <?php
                    $isTurnoverEmpty = TRUE;
                    $y = date(('Y'));
                    for ($i=$y; $i > $y - 5; $i--) {
                        error_log(print_r($value['year-'.$i]['year'], 1));
                        foreach ($value['year-'.$i]['year'] as $key2 => $value2) {
                            if (!empty($value2)) {
                                $isTurnoverEmpty = FALSE;
                            }
                            break;
                        }
                    }
                    if (!$isTurnoverEmpty):
                ?>
                    <div class="table-wrapper col-md-12">
                        <!-- TURNOVER -->
                        <div class="table-title">Company Global Turnover</div>
                        <table class="double-header">
                            <?php $currency = MainSupplierAPI::getCurrency($element['#object']->nid); ?>
                            <thead>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>Total turnover (<?php echo $currency ?>)</td>
                                    <td>Export (%)</td>
                                    <td>Investment (<?php echo $currency ?>)</td>
                                    <td>Investment R&amp;D (<?php echo $currency ?>)</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $y = date(('Y'));
                                    for ($i=$y; $i > $y - 5; $i--) {
                                        $isEmpty = true;
                                        foreach ($value['year-'.$i]['year'] as $key2 => $value2) {
                                            if (!empty($value2)) {
                                                $isEmpty = false;
                                            }
                                            break;
                                        }
                                        if (!$isEmpty) {
                                            echo '<tr><td>'.$i.'</td>';
                                            if (isset($value['year-'.$i]['year']['total_turnover'])) {
                                                echo '<td>'.$value['year-'.$i]['year']['total_turnover'].'</td>';
                                            } else {
                                                echo '<td></td>';
                                            }
                                            if (isset($value['year-'.$i]['year']['export_percentage'])) {
                                                echo '<td>'.$value['year-'.$i]['year']['export_percentage'].'</td>';
                                            } else {
                                                echo '<td></td>';
                                            }
                                            if (isset($value['year-'.$i]['year']['investment'])) {
                                                echo '<td>'.$value['year-'.$i]['year']['investment'].'</td>';
                                            } else {
                                                echo '<td></td>';
                                            }
                                            if (isset($value['year-'.$i]['year']['investment_in_rd'])) {
                                                echo '<td>'.$value['year-'.$i]['year']['investment_in_rd'].'</td>';
                                            } else {
                                                echo '<td></td>';
                                            }
                                            echo '</tr>';
                                        }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
