<?php

/**
 * @file field.tpl.php
 * Default template implementation to display the value of a field.
 *
 * This file is not used and is here as a starting point for customization only.
 * @see theme_field()
 *
 * Available variables:
 * - $items: An array of field values. Use render() to output them.
 * - $label: The item label.
 * - $label_hidden: Whether the label display is set to 'hidden'.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - field: The current template type, i.e., "theming hook".
 *   - field-name-[field_name]: The current field name. For example, if the
 *     field name is "field_description" it would result in
 *     "field-name-field-description".
 *   - field-type-[field_type]: The current field type. For example, if the
 *     field type is "text" it would result in "field-type-text".
 *   - field-label-[label_display]: The current label position. For example, if
 *     the label position is "above" it would result in "field-label-above".
 *
 * Other variables:
 * - $element['#object']: The entity to which the field is attached.
 * - $element['#view_mode']: View mode, e.g. 'full', 'teaser'...
 * - $element['#field_name']: The field name.
 * - $element['#field_type']: The field type.
 * - $element['#field_language']: The field language.
 * - $element['#field_translatable']: Whether the field is translatable or not.
 * - $element['#label_display']: Position of label display, inline, above, or
 *   hidden.
 * - $field_name_css: The css-compatible field name.
 * - $field_type_css: The css-compatible field type.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */

if (!function_exists('map_units_of_measure')) {
    function map_units_of_measure($value) {
        $units_of_measure = array(
          'ton' => 'tons',
          'lfm' => 'LFM',
          'm2' => 'm2',
          'm3' => 'm3',
          'l' => 'liters',
          'count' => 'pieces'
        );
        if (isset($units_of_measure[$value])) {
            return $units_of_measure[$value];
        }
        return $value;
    }
}

?>

<div class="<?php print $classes; ?> suppliers-infos-details"<?php print $attributes; ?>>
    <?php if (!$label_hidden): ?>
        <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?></div>
    <?php endif; ?>
    <div class="field-items"<?php print $content_attributes; ?>>
        <?php foreach($items as $key => $value): ?>
            <div class="row field-item <?php print $key % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$key]; ?>>
                <?php
                    $form = drupal_get_form('_main_supplier_production_information_form');
                    $item = json_decode($value['#markup'], TRUE);
                    $currency = MainSupplierAPI::getCurrency($element['#object']->nid);
                ?>
                <?php
                if (isset($item['human_resource']) && !empty($item['human_resource'])):
                    $employeesCount = 0;
                    $managementCount = 0;
                ?>
                    <div class="table-wrapper col-md-12">
                        <div class="table-title"><?php echo $form['fieldset_1']['#title']; ?></div>
                        <table class="top-header">
                            <tr>
                                <td></td>
                                <?php foreach ($item['human_resource'] as $key2 => $value2) : ;?>
                                    <td><?php echo isset($form['fieldset_1']['human_resource'][0]['type']['#options'][$value2['type']]) && !empty($value2['type']) ? $form['fieldset_1']['human_resource'][0]['type']['#options'][$value2['type']] : (isset($value2['type']) ? $value2['type'] : ''); ?></td>
                                <?php endforeach; ?>
                                <td class="total-cell">Total</td>
                            </tr>

                            <tr>
                                <?php
                                    $total_employees = 0;
                                    $total_delta = array();
                                ?>
                                <td>Employees</td>
                                <?php foreach ($item['human_resource'] as $key2 => $value2) : ?>
                                    <td><?php echo $value2['employees']; ?></td>
                                    <?php
                                    $total_employees += $value2['employees'];
                                    $total_delta[$key2] = $value2['employees']
                                    ?>
                                <?php endforeach; ?>
                                <td class="total-cell"><?php echo $total_employees; ?></td>
                            </tr>
                            <tr>
                                <?php $total_management = 0; ?>
                                <td>Management</td>
                                <?php foreach ($item['human_resource'] as $key2 => $value2) : ?>
                                    <td><?php echo $value2['management']; ?></td>
                                    <?php
                                        $total_management += $value2['management'];
                                        $total_delta[$key2] += $value2['management']
                                    ?>
                                <?php endforeach; ?>
                                <td class="total-cell"><?php echo $total_management; ?></td>
                            </tr>
                            <tr>
                                <td class="total-cell">Total</td>
                                <?php foreach ($total_delta as $value) : ?>
                                    <td class="total-cell"><?php print $value; ?></td>
                                <?php endforeach; ?>
                                <td class="total-cell"><?php print ($total_employees + $total_management); ?></td>
                            </tr>
                        </table>
                    </div>
                <?php endif; ?>
                <?php if (isset($item['production_capacity']) && !empty($item['production_capacity'])): ?>
                    <div class="table-wrapper col-md-12">
                        <div class="table-title"><?php echo $form['fieldset_2']['#title'] ?></div>
                        <table class="top-header">
                            <thead>
                                <tr>
                                    <td>Main product families</td>
                                    <td>Manufacturer in % of your total activity</td>
                                    <td>Trader in % of your total activity</td>
                                    <td>Exclusive / Non exclusive Agent (in % of your total activity)</td>
                                    <td>Owned Production Unit : annual production capacity</td>
                                    <td>Owned Production Unit : Last year unit sold</td>
                                    <td>TRADING Only : Last year Unit sold</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($item['production_capacity'] as $key2 => $value2): ?>
                                    <tr>
                                        <td>
                                            <?php
                                                $term = taxonomy_term_load($value2['product_subfamilies']);
                                                echo $term->name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['manufacturer_share'])) {
                                                    echo $value2['manufacturer_share'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['trader_share'])) {
                                                    echo $value2['trader_share'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['agent_share'])) {
                                                    echo $value2['agent_share'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['owned_production_unit_annual'])) {
                                                    echo $value2['owned_production_unit_annual'].' '.map_units_of_measure($value2['owned_production_unit_annual_measure']);
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['owned_production_unit_last_year'])) {
                                                    echo $value2['owned_production_unit_last_year'].' '.map_units_of_measure($value2['owned_production_unit_last_year_measure']);
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['owned_production_unit_last_year_trading_only'])) {
                                                    echo $value2['owned_production_unit_last_year_trading_only'].' '.map_units_of_measure($value2['owned_production_unit_last_year_trading_only_measure']);
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <?php if (isset($item['storage_capacity']) && !empty($item['storage_capacity'])): ?>
                    <div class="table-wrapper col-md-12">
                        <div class="table-title"><?php echo $form['fieldset_3']['#title'] ?></div>
                        <table class="top-header">
                            <thead>
                                <tr>
                                    <td>Storage capacity per warehouse / factory</td>
                                    <td>Location of your storage unit</td>
                                    <td>Storage capacity in production unit</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $countries = country_get_list();
                                    foreach ($item['storage_capacity'] as $key2 => $value2):
                                ?>
                                    <tr>
                                        <td>
                                            <?php
                                                if (isset($value2['warehouse_factory_name'])) {
                                                    echo $value2['warehouse_factory_name'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (!empty($value2['location'])) {
                                                    echo $countries[$value2['location']];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['storage_capacity'])) {
                                                    echo $value2['storage_capacity'].' '.map_units_of_measure($value2['storage_capacity_measure']);
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
                <?php if (!empty($item['production_sites_count']) || !empty($item['surface']) || !empty($item['own_units_production_part']) || !empty($item['sub_contracted_production_part'])): ?>
                    <div class="table-wrapper col-md-12">
                        <div class="table-title"><?php echo $form['fieldset_4']['#title']; ?></div>
                        <table>
                            <?php
                                if (isset($item['production_sites_count']) && !empty($item['production_sites_count'])) {
                                    echo '<tr><td>Number of production units owned</td><td>'.$item['production_sites_count'].'</td></tr>';
                                }
                                if (isset($item['surface']) && !empty($item['surface'])) {
                                    echo '<tr><td>Total owned production surface in m2</td><td>'.$item['surface'].'</td></tr>';
                                }
                                if (isset($item['own_units_production_part']) && !empty($item['own_units_production_part'])) {
                                    echo '<tr><td>% produced in your units of your global activity</td><td>'.$item['own_units_production_part'].'</td></tr>';
                                }
                                if (isset($item['sub_contracted_production_part']) && !empty($item['sub_contracted_production_part'])) {
                                    echo '<tr><td>% sub-contracted production</td><td>'.$item['sub_contracted_production_part'].'</td></tr>';
                                }
                            ?>
                        </table>
                    </div>
                <?php endif; ?>
                <?php if (isset($item['factory_detail']) && !empty($item['factory_detail'])): ?>
                    <div class="table-wrapper col-md-12">
                        <div class="table-title"><?php echo $form['fieldset_5']['#title'] ?></div>
                        <table class="top-header">
                            <thead>
                                <td>Factory name</td>
                                <td>Country</td>
                                <td>Factory location city</td>
                                <td>Own factory / partnership</td>
                                <td>Main Product Family produced</td>
                                <td>Production Capacity</td>
                                <td>% Production Occupancy</td>
                                <td>Production Lead Time (Average, in open days)</td>
                                <td>Last year Turnover in <?php echo $currency; ?></td>
                            </thead>
                            <tbody>
                                <?php
                                    $countries = country_get_list();
                                    foreach ($item['factory_detail'] as $key2 => $value2): ?>
                                    <tr>
                                        <td>
                                            <?php
                                                if (isset($value2['factory_name'])) {
                                                    echo $value2['factory_name'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (!empty($value2['country'])) {
                                                    echo $countries[$value2['country']];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['city'])) {
                                                    echo $value2['city'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['own_factory_partnership'])) {
                                                    echo $value2['own_factory_partnership'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['product_options'])) {
                                                    echo $value2['product_options'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['production_capacity'])) {
                                                    echo $value2['production_capacity'].' '.map_units_of_measure($value2['production_capacity_measure']);
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['production_occupancy'])) {
                                                    echo $value2['production_occupancy'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['production_lead_time'])) {
                                                    echo $value2['production_lead_time'];
                                                }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                if (isset($value2['last_year_turnover'])) {
                                                    echo $value2['last_year_turnover'];
                                                }
                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                <?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
</div>
