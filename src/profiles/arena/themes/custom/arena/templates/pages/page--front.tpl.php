<div class="l-page">
    <?php print $messages; ?>
    <?php require $header ?>
    <div class="l-main">
        <div class="l-content" role="main">
            <a id="main-content"></a>
            <?php print render($page['content']); ?>
        </div>
    </div>
    <?php require $footer ?>
</div>
