<footer class="l-footer" role="contentinfo">
    <div class="l-footer-inner clearfix">
        <div class="l-branding">
            <?php if ($footer_logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"
                   class="footer-logo"><img src="<?php print $footer_logo; ?>" alt="<?php print t('Home'); ?>"/></a>
            <?php endif; ?>
        </div>
        <?php print render($page['footer']); ?>
    </div>
</footer>
