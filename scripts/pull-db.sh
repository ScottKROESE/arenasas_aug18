#!/bin/bash


# /!\ This script needs the bc package installed on the server.
# Helper to let you run the install script from anywhere.
currentscriptpath () {
  SOURCE="${BASH_SOURCE[0]}"
  # resolve $SOURCE until the file is no longer a symlink
  while [ -h "$SOURCE" ]; do

    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
  done
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  echo $DIR
}

RESULT=$(currentscriptpath)
ROOT=$RESULT"/../"

cd $ROOT

echo "Project directory: "`pwd`

if [ ! -d htdocs ]; then
  echo 'Error.'
  exit;
fi


read -r -p "Drop local Database ? [y/N] " response
case $response in
    [yY][eE][sS]|[yY])
        ;;
    *)
        echo "Operation aborted..."
        exit
        ;;
esac

wget -P arena-private-files/ http://buzzaka:Buzz06\$dev@development.arena.buzzaka.net/dumps/dump-arena-development-last.sql.gz
cd htdocs/
drush sql-drop --yes
gunzip -c ../arena-private-files/dump-arena-development-last.sql.gz | drush sql-cli
rm ../arena-private-files/dump-arena-development-last.sql.gz
