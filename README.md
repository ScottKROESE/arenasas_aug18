# ARENA

## Preprod :

### Mail en preprod

**Les mails de preprod ne sont plus envoyé** à la place ils sont stocker dans des fichiers dans un répertoire, voici l'url pour voir les mails envoyé de preprod :

http://195.46.203.216/sites/default/sendmails/

ou

http://developmentsite.alliance-arena.com/sites/default/sendmails/

Pour que cela fonctionne il faut que le module Devel soit activé.
Et nous avons ajouté ça :

```
#!php
  // Use Devel's maillog
  $conf['mail_system'] = array( 
    'default-system' => 'DevelMailLog',
  );
  // To set custom path 
  $conf['devel_debug_mail_directory'] = 'sites/default/sendmails';
```

Dans le fichier settings.local.php de la preprod.